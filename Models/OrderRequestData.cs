using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class OrderRequestData : BaseOrderRequestData{

        [JsonProperty(PropertyName = "terminal")]
        public string Terminal { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public int Amount { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "orderReference")]
        public string OrdeReference { get; set; }
    }
}