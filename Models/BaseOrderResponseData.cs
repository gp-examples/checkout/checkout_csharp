using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class BaseOrderResponseData {
        [JsonProperty(PropertyName = "session")]
        public string Session { get; set; }

        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
    }
}