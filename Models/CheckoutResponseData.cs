using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class CheckoutResponseData {
        [JsonProperty(PropertyName = "status")]
        public int Status { get; set; }

        [JsonProperty(PropertyName = "orderId")]
        public string OrderId { get; set; }

        [JsonProperty(PropertyName = "authorization")]
        public string Authorization { get; set; }

        [JsonProperty(PropertyName = "last4")]
        public string Last4 { get; set; }

        [JsonProperty(PropertyName = "brand")]
        public string Brand { get; set; }

        [JsonProperty(PropertyName = "result")]
        public CheckoutResponseDataResult Result { get; set; }

        [JsonProperty(PropertyName = "_signature")]
        public string Signature { get; set; }
    }
}