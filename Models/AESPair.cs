using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class AESPair {
        [JsonProperty(PropertyName = "k")]
        public int[] Key { get; set; }

        [JsonProperty(PropertyName = "s")]
        public int Counter { get; set; }
    }
}