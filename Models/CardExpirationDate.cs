using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class CardExpirationDate {
        [JsonProperty(PropertyName = "month")]
        public int Month { get; set; }

        [JsonProperty(PropertyName = "year")]
        public int Year { get; set; }
    }
}