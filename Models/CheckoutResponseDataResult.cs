using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class CheckoutResponseDataResult {
        [JsonProperty(PropertyName = "time_local_tran")]
        public string TimeLocalTran { get; set; }

        [JsonProperty(PropertyName = "systems_trace_audit_number")]
        public string SystemsTraceAuditNumber { get; set; }
        
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }
        
        [JsonProperty(PropertyName = "retrieval_ref_num")]
        public string RetrievalRefNum { get; set; }

        [JsonProperty(PropertyName = "resp_code")]
        public string RespCode { get; set; }

        [JsonProperty(PropertyName = "reserved_private4")]
        public string ReservedPrivate4 { get; set; }

        [JsonProperty(PropertyName = "proc_code")]
        public string ProcCode { get; set; }

        [JsonProperty(PropertyName = "network_international_id")]
        public string NetworkInternationalId { get; set; }

        [JsonProperty(PropertyName = "mti")]
        public string Mti { get; set; }

        [JsonProperty(PropertyName = "merchant_id")]
        public string MerchantId { get; set; }

        [JsonProperty(PropertyName = "date_local_tran")]
        public string DateLocalTran { get; set; }

        [JsonProperty(PropertyName = "card_acceptor_terminal_id")]
        public string CardAcceptorTerminalId { get; set; }

        [JsonProperty(PropertyName = "authorization_id_resp")]
        public string AuthorizationIdResp { get; set; }
    }
}