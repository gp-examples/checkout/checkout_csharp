using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class TokenizedCard : CreditCard{
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
    }
}