using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class TokenizeCardCheckoutResponseData {
        [JsonProperty(PropertyName = "status")]
        public int Status { get; set; }

        [JsonProperty(PropertyName = "expiration_date")]
        public string ExpirationDate { get; set; }

        [JsonProperty(PropertyName = "brand")]
        public string Brand { get; set; }

        [JsonProperty(PropertyName = "nickname")]
        public string Nickname { get; set; }

        [JsonProperty(PropertyName = "callback")]
        public string Callback { get; set; }

        [JsonProperty(PropertyName = "result")]
        public TokenizeCardCheckoutResponseDataResult Result { get; set; }

        [JsonProperty(PropertyName = "_signature")]
        public string Signature { get; set; }
    }
}