using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class CardInfo {
        [JsonProperty(PropertyName = "cardHolder")]
        public string CardHolder { get; set; }

        [JsonProperty(PropertyName = "expirationDate")]
        public CardExpirationDate CardExpirationDate { get; set; }

        [JsonProperty(PropertyName = "cardNumber")]
        public string CardNumber { get; set; }

        [JsonProperty(PropertyName = "cvc")]
        public string CVC { get; set; }

        [JsonProperty(PropertyName = "nickname")]
        public string Nickname { get; set; }
    }
}