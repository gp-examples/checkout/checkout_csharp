using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class CheckoutRequestData {
        [JsonProperty(PropertyName = "session")]
        public string Session { get; set; }

        [JsonProperty(PropertyName = "ld")]
        public string LD { get; set; }

        [JsonProperty(PropertyName = "lk")]
        public string LK { get; set; }
    }
}