using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class BaseOrderRequestData {
        [JsonProperty(PropertyName = "secret")]
        public string Secret { get; set; }

        [JsonProperty(PropertyName = "merchantId")]
        public string MerchantId { get; set; }
    }
}