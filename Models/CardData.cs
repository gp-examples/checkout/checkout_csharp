using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class CardData : CreditCard{
        [JsonProperty(PropertyName = "card")]
        public CardInfo CardInfo { get; set; }
    }
}