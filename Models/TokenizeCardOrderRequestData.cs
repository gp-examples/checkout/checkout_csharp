using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class TokenizeCardOrderRequestData : BaseOrderRequestData{

        [JsonProperty(PropertyName = "requestId")]
        public string RequestId { get; set; }
    }
}