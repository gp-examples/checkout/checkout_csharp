
using Newtonsoft.Json;

namespace checkout_csharp.Models{
    public class TokenizeCardCheckoutResponseDataResult {
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }

        [JsonProperty(PropertyName = "last_digits")]
        public string LastDigits { get; set; }
        
        [JsonProperty(PropertyName = "bin")]
        public string Bin { get; set; }
    }
}