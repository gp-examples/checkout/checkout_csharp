# Checkout Greenpay Example in C#

This is a C# .Net Core 2.1 Console App showing a checkout process in Greenpay.

To run this example you need do the folowing:
1. Download or clone this repo.
2. Edit the file appsettings.json and fill the credentials provided by Greenpay support team.
3. Open checkout_csharp.cs en Visual Studio or Visual Studio Code.
4. Run the project.