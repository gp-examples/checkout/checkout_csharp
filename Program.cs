﻿using checkout_csharp.AES;
using checkout_csharp.Models;
using checkout_csharp.RSA;
using checkout_csharp.Utils;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;

using Microsoft.Extensions.Configuration;


namespace checkout_csharp
{
    class Program
    {
        private static HttpClient client = new HttpClient();

        public static OrderRequestData orderRequestData = null;
        public static CardData cardData = null;

        public static TokenizeCardOrderRequestData tokenizeCardOrderRequestData = null;

        public static string publicKeyFileName = "";
        public static string createOrderUrl = "";
        public static string checkoutUrl = "";
        public static string tokenizeCardCreateOrderUrl = "";
        public static string tokenizeCardCheckoutUrl = "";

        public static void Main(string[] args)
        {
            GenerateTestData();
            StartGreenPayProcess().Wait();
        }

        public static IConfigurationRoot ReadAppSettings(){
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            //Console.WriteLine(configuration["Test"]);   
            return configuration;
        }

        /**
            Nombre: GenerateTestData
            Descripción: Genera los datos de prueba.
        */
        public static void GenerateTestData(){

            var config = ReadAppSettings();

            publicKeyFileName = config["GreenPayCredentials:PublicKeyFileName"];
            createOrderUrl =  config["GreenPayCredentials:MerchantEndpoint"];
            checkoutUrl = config["GreenPayCredentials:CheckoutEndpoint"];
            tokenizeCardCreateOrderUrl = config["GreenPayCredentials:TokenizeCardMerchantEndpoint"];
            tokenizeCardCheckoutUrl = config["GreenPayCredentials:TokenizeCardCheckoutEndpoint"];

            orderRequestData = new OrderRequestData(){
                Secret= config["GreenPayCredentials:Secret"],
                MerchantId=config["GreenPayCredentials:MerchantId"],
                Terminal=config["GreenPayCredentials:Terminal"],
                Amount=1,
                Currency="",
                Description="desc",
                OrdeReference="1",
            };

            cardData = new CardData(){
                CardInfo = new CardInfo(){
                    CardHolder="Jhon Doe",
                    CardExpirationDate = new CardExpirationDate(){
                        Month=9,
                        Year=21
                    },
                    CardNumber="4777777777777777",
                    CVC="123",
                    Nickname="visa2449"
                }
            };

            tokenizeCardOrderRequestData = new TokenizeCardOrderRequestData(){
                Secret= config["GreenPayCredentials:Secret"],
                MerchantId=config["GreenPayCredentials:MerchantId"],
                RequestId="1",
            };
        }

        /**
            Nombre: StartGreenPayProcess
            Descripción: Comienza el proceso de compra contra la plataforma Greenpay.
        */
        private static async Task StartGreenPayProcess()
        {
            // El orden para ejecutar la transacción:
            // 1. Crear una nueva orden.
            // 2. Realizar el proceso de Checkout de la orden.
            // 3. Verificar la firma de la respuesta de Greenpay para garantizar la integridad del mensaje.
            OrderResponseData orderResponseData = await PostCreateOrder(orderRequestData);
            if (orderRequestData != null){
                CheckoutResponseData checkoutResponseData = await PostCheckout(cardData,orderResponseData);
                bool verify = VerifyCheckoutResponse(checkoutResponseData);
            }

            // El orden para realizar la tokenización de una tarjeta:
            // 1. Crear una orden para tokenizar la tarjeta.
            // 2. Realizar el proceso de Checkout para la orden de tokenización.
            // 3. Verificar la firma de la respuesta de GreenPay para garantizar la integridad del mensaje.
            TokenizeCardOrderResponseData tokenizeCardOrderResponseData = await PostTokenizeCardCreateOrder(tokenizeCardOrderRequestData);
            if (tokenizeCardOrderResponseData != null){
                TokenizeCardCheckoutResponseData tokenizeCardcheckoutResponseData = await PostTokenizeCardCheckout(cardData,tokenizeCardOrderResponseData);
                bool tokenizeCarVerify = VerifyTokenizeCardCheckoutResponse(tokenizeCardcheckoutResponseData, tokenizeCardOrderRequestData.RequestId);

                // El orden para realizar la el pago de una orden con una tarjta tokenizada:
                // 1. Obtener el token de la tarjeta previamente tokenizada.
                // 2. Crear una orden para tokenizar la tarjeta.
                // 3. Realizar el proceso de Checkout para la orden de tokenización.
                // 4. Verificar la firma de la respuesta de GreenPay para garantizar la integridad del mensaje.
                TokenizedCard tokenizedCard = new TokenizedCard(){
                    Token = tokenizeCardcheckoutResponseData.Result.Token
                };
                OrderResponseData tokenizedCardOrderResponseData = await PostCreateOrder(orderRequestData);
                if (orderRequestData != null){
                    CheckoutResponseData tokenizedCardCheckoutResponseData = await PostCheckout(tokenizedCard,tokenizedCardOrderResponseData);
                    bool checkoutTokenizeCardVerify = VerifyCheckoutResponse(tokenizedCardCheckoutResponseData);
                }
            }
        }

        /**
            Nombre: PostCreateOrder
            Descripción: Realiza el proceso de creación de una nueva orden en la plataforma Greenpay.
            Retorna: El objeto OrderResponseData que contiene la respuesta a la creación de la orden.
            Parámetros: - OrderResponseData: Objeto OrderRequestData con los datos para crear una nueva orden.
        */
        public static async Task<OrderResponseData> PostCreateOrder(OrderRequestData OrderResponseData){
            OrderResponseData orderResponseData = null;
            string requestBody = JsonConvert.SerializeObject(orderRequestData);
            HttpContent bodyContent = new StringContent(requestBody);
            bodyContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = await client.PostAsync(createOrderUrl, bodyContent);
            if (response.IsSuccessStatusCode){
                string responseText = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Create Order Response: " + responseText);
                orderResponseData = JsonConvert.DeserializeObject<OrderResponseData>(responseText);
            }else{
                Console.WriteLine("Error creating order: " + response.ReasonPhrase);
                return null;
            }
            return orderResponseData;
        }

        /**
            Nombre: PostCheckout
            Descripción: Realiza el proceso de checkout en la plataforma Greenpay.
            Retorna: El objeto CheckoutResponseData que contiene la respuesta del proceso de Checkout.
            Parámetros: - orderResponseData: Objeto orderResponseData con los datos para hacer checkout.
        */
        public static async Task<CheckoutResponseData> PostCheckout(CreditCard cardData, OrderResponseData orderResponseData){
            Console.WriteLine("Checkout data: " + JsonConvert.SerializeObject(cardData));
            CheckoutResponseData checkoutReponseData = null;

            CheckoutRequestData checkoutRequestData = Pack(cardData, orderResponseData);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, checkoutUrl);
            requestMessage.Headers.Add("Accept", "application/json");
            // Se debe incluir el header liszt-token con el token que retorna el servicio de creación de nueva orden en Greenpay.
            requestMessage.Headers.Add("liszt-token", orderResponseData.Token);
            
            string requestBody = JsonConvert.SerializeObject(checkoutRequestData);
            Console.WriteLine("Checkout Request: " + requestBody);
            HttpContent bodyContent = new StringContent(requestBody);
            bodyContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            requestMessage.Content = bodyContent;

            HttpResponseMessage response = await client.SendAsync(requestMessage);
            if (response.IsSuccessStatusCode){
                string responseText = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Checkout Response: " + responseText);
                checkoutReponseData = JsonConvert.DeserializeObject<CheckoutResponseData>(responseText);
            }else{
                Console.WriteLine("Error In Checkout: " + response.ReasonPhrase);
            }

            return checkoutReponseData;
        }
        
        /**
            Nombre: VerifyCheckoutResponse
            Descripción: Valida que la respuesta obtenida por el proceso de Checkout de Greenpay fue generada en los servidores de Greenpay y no ha sido manipulada por terceras partes.
            Retorna: True si la firma de la respuesta del proceso de Checkout en Greenpay es correcta. Falso en caso contrario.
            Parámetros: - checkoutResponseData: Objeto con la respuesta del proceso de Checkout en Greenpay que contiene la firma a validar.
        */
        public static bool VerifyCheckoutResponse(CheckoutResponseData checkoutResponseData){
            // La firma contiene el status de la transacción y el número de orden, en el siguiente formato:
            string verifyValue = "status:" + checkoutResponseData.Status + ",orderId:" + checkoutResponseData.OrderId;
            bool verified = RSABouncyCastle.VerifySignatureWithPublicKeyFromFile(verifyValue, checkoutResponseData.Signature, publicKeyFileName);
            if (verified){
                Console.WriteLine("Signature verified");
            }else{
                Console.WriteLine("Signature NOT verified");
            }
            return verified;
        }

        /**
            Nombre: Pack
            Descripción: Realiza los cálculos y procesamiento de datos para generar el objeto CheckoutRequestData, requerido para hacer el proceso de Checkout en Greenpay.
            Retorna: El objeto CheckoutRequestData que contiene todos los datos requeridos para realizar el proces de Checkout en Greenpay.
            Parámetros: - cardData: Objeto con los datos de la tarjeta con la que se desea pagar.
                        - orderResponseData: Objeto que contiene la respuesta de la creación de la orden en Greenpay.
        */
        public static CheckoutRequestData Pack(CreditCard cardData, BaseOrderResponseData baseOrderResponseData){
            // 1. Generar la llave para el cifrado AES en modo CTR
            AESKey aesKey = CipherUtils.GenerateAESKey();
            byte[] keyBytes = aesKey.GetKeyValue();

            // 2. Generar el contador (Counter) para el cifrado AES en modo CTR
            AESCounter aesCounter = CipherUtils.GenerateAESCounter();
            byte[] counterBytes = aesCounter.GetCounterValue();

            // 3. Convertir los datos del objeto que contiene la tarjeta a string y luego a bytes
            string cardDataStr = JsonConvert.SerializeObject(cardData);
            byte[] cardDataBytes = System.Text.Encoding.UTF8.GetBytes(cardDataStr);

            // 4. Crear los objetos de tipo Stream para leer y escribir los datos cifrados.
            Stream cardDataStream = new MemoryStream(cardDataBytes);
            Stream outputStream = new MemoryStream();

            // 5    ### Cifrado con AES ###

            // 5.1 Opción #1:
            //  Utilizar una implementación manual de AES en modo CTR para cifrar los datos de la tarjeta. Esta implementación utiliza 
            //  clases nativas de .Net:
            //  - La llave AES generada representa en un arreglo de bytes.
            //  - El contador utilizado para el modo CTR representado en un arreglo de bytes.
            //  - El stream de entrada que contiene los datos a cifrar (datos de la tarjeta)
            //  - El stream de salida que va a contener el texto cifrado con AES en modo CTR.
            AESCTRMode.CipherAESCTR(keyBytes, counterBytes, cardDataStream, outputStream);

            // 5.1.1    Convertir el MemoryStream con el contenido cifrado en AES modo CTR y representarlo como cadena hexadecimal.
            byte[] resultAsArray = ((MemoryStream)outputStream).ToArray();
            string hexResult = StringUtils.ByteArrayToHexString(resultAsArray); // LD

            // 5.2 Opción #2:
            //  Utilizar la librería Bouncy Castle para realizar el cifrado utilizando:
            //  - La llave AES generada representa en un arreglo de bytes.
            //  - El contador utilizado para el modo CTR representado en un arreglo de bytes.
            //  - El arreglo de bytes que representa los datos a cifrar.
            resultAsArray = AESBouncyCastle.CipherAESCRTWithBouncyCastle(keyBytes, counterBytes, cardDataBytes);
            
            // 5.2.1    Convertir el arreglo de bytes con contenido cifrado en AES modo CTR y representarlo como cadena hexadecimal.
            hexResult = StringUtils.ByteArrayToHexString(resultAsArray); // LD


            // 6.   Obtener el valor LK (Liszt Key) requerido para la trama de Checkout.
            // 6.1  Se deben guardar los valores de la llave (como un arreglo de bytes) y el counter (como un valor nunérico entero).
            // 6.2  Al serializado los dos valores se obtiene algo como:
            //                      "{\"k\":[98,191,20,0,70,53,133,100,8,131,110,91,79,186,218,166],\"s\":138}"  
            //      En donde el valor "k" contiene la llave AES y el valor "s" contiene el counter usados para cifrar AES en modo CTR del paso 5.
            // 6.3  Utilizar el cifrado asimétrico RSA con la llave pública brindada por Greenpay para cifrar la trama de 6.2
            //      El resultado se encuentra representado en base64.
            AESPair aesPair = new AESPair();
            aesPair.Key = aesKey.Key;
            aesPair.Counter = aesCounter.Counter;
            string aesPairStr = JsonConvert.SerializeObject(aesPair);
            string lk = RSABouncyCastle.EncryptRSAWithPublicKeyFromFile(aesPairStr, publicKeyFileName);

            // 8. Crea la entidad CheckoutRequestData con los datos de 6 y 7.
            CheckoutRequestData checkoutRequest = new CheckoutRequestData();
            checkoutRequest.Session = baseOrderResponseData.Session;    // Valor de sesión obtenida de la respuesta de Crear Orden en Greenpay.
            checkoutRequest.LD = hexResult; // Datos de la tarjeta cifrado con AES en modo CTR representados como una cadena Hexadecimal (Paso 5.1.1 ó 5.2.1.)
            checkoutRequest.LK = lk;    // Datos de las llaves de cifrado AES en modo CTR (tanto la llave como el contador), cifrados con RSA con la llave pública. Resultado en formato base64

            return checkoutRequest;
        }
        
        /**
            Nombre: PostTokenizeCardCreateOrder
            Descripción: Realiza el proceso de creación de una nueva orden para tokenizar una tarjeta en GreenPay.
            Retorna: El objeto TokenizeCardOrderResponseData que contiene la respuesta a la creación de la orden.
            Parámetros: - TokenizeCardOrderRequestData: Objeto TokenizeCardOrderRequestData con los datos para crear una nueva orden de tokenizaciómn de tarjeta.
        */
        public static async Task<TokenizeCardOrderResponseData> PostTokenizeCardCreateOrder(TokenizeCardOrderRequestData OrderResponseData){
            TokenizeCardOrderResponseData tokenizCardOrderResponseData = null;
            string requestBody = JsonConvert.SerializeObject(orderRequestData);
            HttpContent bodyContent = new StringContent(requestBody);
            bodyContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = await client.PostAsync(tokenizeCardCreateOrderUrl, bodyContent);
            if (response.IsSuccessStatusCode){
                string responseText = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Tokenize Card Create Order Response: " + responseText);
                tokenizCardOrderResponseData = JsonConvert.DeserializeObject<TokenizeCardOrderResponseData>(responseText);
            }else{
                Console.WriteLine("Error creating order for tokenize card: " + response.ReasonPhrase);
                return null;
            }
            return tokenizCardOrderResponseData;
        }

        /**
            Nombre: PostTokenizeCardCheckout
            Descripción: Realiza el proceso de checkout en la plataforma Greenpay para tokenizar tarjetas.
            Retorna: El objeto TokenizeCardCheckoutResponseData que contiene la respuesta del proceso de Checkout para tokenizar tarjetas.
            Parámetros: - tokenizeCardOrderResponseData: Objeto tokenizeCardOrderResponseData con los datos para hacer checkout para tokenizar tarjetas.
        */
        public static async Task<TokenizeCardCheckoutResponseData> PostTokenizeCardCheckout(CardData cardData, TokenizeCardOrderResponseData tokenizeCardOrderResponseData){
            Console.WriteLine("Tokenize Card Checkout data: " + JsonConvert.SerializeObject(cardData));
            TokenizeCardCheckoutResponseData tokenizeCardCheckoutReponseData = null;

            CheckoutRequestData checkoutRequestData = Pack(cardData, tokenizeCardOrderResponseData);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, tokenizeCardCheckoutUrl);
            requestMessage.Headers.Add("Accept", "application/json");
            // Se debe incluir el header liszt-token con el token que retorna el servicio de creación de nueva orden en Greenpay.
            requestMessage.Headers.Add("liszt-token", tokenizeCardOrderResponseData.Token);
            
            string requestBody = JsonConvert.SerializeObject(checkoutRequestData);
            Console.WriteLine("Tokenize Card Checkout Request: " + requestBody);
            HttpContent bodyContent = new StringContent(requestBody);
            bodyContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            requestMessage.Content = bodyContent;

            HttpResponseMessage response = await client.SendAsync(requestMessage);
            if (response.IsSuccessStatusCode){
                string responseText = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Tokenize Card Checkout Response: " + responseText);
                tokenizeCardCheckoutReponseData = JsonConvert.DeserializeObject<TokenizeCardCheckoutResponseData>(responseText);
            }else{
                Console.WriteLine("Error In Tokenize Card Checkout: " + response.ReasonPhrase);
            }

            return tokenizeCardCheckoutReponseData;
        }

        /**
            Nombre: VerifyTokenizeCardCheckoutResponse
            Descripción: Valida que la respuesta obtenida por el proceso de Checkout de Tarjeta Tokenizada de Greenpay fue generada en los servidores de Greenpay y no ha sido manipulada por terceras partes.
            Retorna: True si la firma de la respuesta del proceso de Checkout de Tarjeta Tokenizada en Greenpay es correcta. Falso en caso contrario.
            Parámetros: - tokenizeCardCheckoutResponseData: Objeto con la respuesta del proceso de Checkout de tarjeta Tokenizada en Greenpay que contiene la firma a validar.
        */
        public static bool VerifyTokenizeCardCheckoutResponse(TokenizeCardCheckoutResponseData tokenizeCardCheckoutResponseData, string requestId){
            // La firma contiene el status de la transacción y el número de request, en el siguiente formato:
            string verifyValue = "status:" + tokenizeCardCheckoutResponseData.Status + ",requestId:" + requestId;
            bool verified = RSABouncyCastle.VerifySignatureWithPublicKeyFromFile(verifyValue, tokenizeCardCheckoutResponseData.Signature, publicKeyFileName);
            if (verified){
                Console.WriteLine("Tokenize Card Signature verified");
            }else{
                Console.WriteLine("Tokenize Card Signature NOT verified");
            }
            return verified;
        }
    }
}
